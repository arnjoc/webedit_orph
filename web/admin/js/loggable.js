$(document).ready(function(){
	
	$("#entity").click(function(){
		$("#entity_block").hide(600);
		$("#history_block").show(600);
	});
	$("#history").click(function(){
		$("#history_block").hide(600);
		$("#entity_block").show(600);
	});
    $("#selectAll").change(function(){

        	$('.select').each(function(){
			$(this).click();
			});

    });
    $('.select').click(function(){
    	var on=null;
    	$('.select').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$("#submit").show();			
			on=null;
		}
		else
		{
			$("#submit").hide();
		}
    });

    //post trash
	 $("#selectAll_post").change(function(){

        	$('.select_post').each(function(){
			$(this).click();
			});

    });
    $('.select_post').click(function(){
    	var on=null;
    	$('.select_post').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_post").show();
			$(".restore_trash_post").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_post").hide();
			$(".restore_trash_post").hide();	
		}
    });
    $('.submit_trash_post').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Post"}));
		$form.submit();
	});
 	$('.restore_trash_post').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Post"}));
		$form.submit();
	});

 	//album trash
	 $("#selectAll_album").change(function(){

        	$('.select_album').each(function(){
			$(this).click();
			});

    });
    $('.select_album').click(function(){
    	var on=null;
    	$('.select_album').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_album").show();
			$(".restore_trash_album").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_album").hide();
			$(".restore_trash_album").hide();	
		}
    });
    $('.submit_trash_album').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Album"}));
		$form.submit();
	});
 	$('.restore_trash_album').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Album"}));
		$form.submit();
	});

	//tag trash
	 $("#selectAll_tag").change(function(){

        	$('.select_tag').each(function(){
			$(this).click();
			});

    });
    $('.select_tag').click(function(){
    	var on=null;
    	$('.select_tag').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_tag").show();
			$(".restore_trash_tag").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_tag").hide();
			$(".restore_trash_tag").hide();	
		}
    });
    $('.submit_trash_tag').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Tag"}));
		$form.submit();
	});
 	$('.restore_trash_tag').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Tag"}));
		$form.submit();
	});

	//service trash
	 $("#selectAll_service").change(function(){

        	$('.select_service').each(function(){
			$(this).click();
			});

    });
    $('.select_service').click(function(){
    	var on=null;
    	$('.select_service').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_service").show();
			$(".restore_trash_service").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_service").hide();
			$(".restore_trash_service").hide();	
		}
    });
    $('.submit_trash_service').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Service"}));
		$form.submit();
	});
 	$('.restore_trash_service').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Service"}));
		$form.submit();
	});

	//team trash
	 $("#selectAll_team").change(function(){

        	$('.select_team').each(function(){
			$(this).click();
			});

    });
    $('.select_team').click(function(){
    	var on=null;
    	$('.select_team').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_team").show();
			$(".restore_trash_team").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_team").hide();
			$(".restore_trash_team").hide();	
		}
    });
    $('.submit_trash_team').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Team"}));
		$form.submit();
	});
 	$('.restore_trash_team').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Team"}));
		$form.submit();
	});

 	//user trash
	 $("#selectAll_user").change(function(){

        	$('.select_user').each(function(){
			$(this).click();
			});

    });
    $('.select_user').click(function(){
    	var on=null;
    	$('.select_user').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_user").show();
			$(".restore_trash_user").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_user").hide();
			$(".restore_trash_user").hide();	
		}
    });
    $('.submit_trash_user').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:User"}));
		$form.submit();
	});
 	$('.restore_trash_user').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:User"}));
		$form.submit();
	});


	//slider trash
	 $("#selectAll_slider").change(function(){

        	$('.select_slider').each(function(){
			$(this).click();
			});

    });
    $('.select_slider').click(function(){
    	var on=null;
    	$('.select_slider').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_slider").show();
			$(".restore_trash_slider").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_slider").hide();
			$(".restore_trash_slider").hide();	
		}
    });
    $('.submit_trash_slider').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Slider"}));
		$form.submit();
	});
 	$('.restore_trash_slider').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Slider"}));
		$form.submit();
	});

 	//sliderimage trash
	 $("#selectAll_sliderimage").change(function(){

        	$('.select_sliderimage').each(function(){
			$(this).click();
			});

    });
    $('.select_sliderimage').click(function(){
    	var on=null;
    	$('.select_sliderimage').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_sliderimage").show();
			$(".restore_trash_sliderimage").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_sliderimage").hide();
			$(".restore_trash_sliderimage").hide();	
		}
    });
    $('.submit_trash_sliderimage').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Sliderimage"}));
		$form.submit();
	});
 	$('.restore_trash_sliderimage').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Sliderimage"}));
		$form.submit();
	});



 	//regulation trash
	 $("#selectAll_regulation").change(function(){

        	$('.select_regulation').each(function(){
			$(this).click();
			});

    });
    $('.select_regulation').click(function(){
    	var on=null;
    	$('.select_regulation').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_regulation").show();
			$(".restore_trash_regulation").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_regulation").hide();
			$(".restore_trash_regulation").hide();	
		}
    });
    $('.submit_trash_regulation').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Regulation"}));
		$form.submit();
	});
 	$('.restore_trash_regulation').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Regulation"}));
		$form.submit();
	});


	//category trash
	 $("#selectAll_category").change(function(){

        	$('.select_category').each(function(){
			$(this).click();
			});

    });
    $('.select_category').click(function(){
    	var on=null;
    	$('.select_category').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_category").show();
			$(".restore_trash_category").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_category").hide();
			$(".restore_trash_category").hide();	
		}
    });
    $('.submit_trash_category').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Category"}));
		$form.submit();
	});
 	$('.restore_trash_category').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Category"}));
		$form.submit();
	});

	//partner trash
	 $("#selectAll_partner").change(function(){

        	$('.select_partner').each(function(){
			$(this).click();
			});

    });
    $('.select_partner').click(function(){
    	var on=null;
    	$('.select_partner').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_partner").show();
			$(".restore_trash_partner").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_partner").hide();
			$(".restore_trash_partner").hide();	
		}
    });
    $('.submit_trash_partner').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Partner"}));
		$form.submit();
	});
 	$('.restore_trash_partner').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Partner"}));
		$form.submit();
	});


	//faq trash
	 $("#selectAll_faq").change(function(){

        	$('.select_faq').each(function(){
			$(this).click();
			});

    });
    $('.select_faq').click(function(){
    	var on=null;
    	$('.select_faq').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_faq").show();
			$(".restore_trash_faq").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_faq").hide();
			$(".restore_trash_faq").hide();	
		}
    });
    $('.submit_trash_faq').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Faqs"}));
		$form.submit();
	});
 	$('.restore_trash_faq').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Faqs"}));
		$form.submit();
	});

	//groupefaq trash
	 $("#selectAll_groupefaq").change(function(){

        	$('.select_groupefaq').each(function(){
			$(this).click();
			});

    });
    $('.select_groupefaq').click(function(){
    	var on=null;
    	$('.select_groupefaq').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_groupefaq").show();
			$(".restore_trash_groupefaq").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_groupefaq").hide();
			$(".restore_trash_groupefaq").hide();	
		}
    });
    $('.submit_trash_groupefaq').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Groupefaqs"}));
		$form.submit();
	});
 	$('.restore_trash_groupefaq').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Groupefaqs"}));
		$form.submit();
	});



	//event trash
	 $("#selectAll_event").change(function(){

        	$('.select_event').each(function(){
			$(this).click();
			});

    });
    $('.select_event').click(function(){
    	var on=null;
    	$('.select_event').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_event").show();
			$(".restore_trash_event").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_event").hide();
			$(".restore_trash_event").hide();	
		}
    });
    $('.submit_trash_event').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Event"}));
		$form.submit();
	});
 	$('.restore_trash_event').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Event"}));
		$form.submit();
	});


	//categoryalbum trash
	 $("#selectAll_categoryalbum").change(function(){

        	$('.select_categoryalbum').each(function(){
			$(this).click();
			});

    });
    $('.select_categoryalbum').click(function(){
    	var on=null;
    	$('.select_categoryalbum').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_categoryalbum").show();
			$(".restore_trash_categoryalbum").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_categoryalbum").hide();
			$(".restore_trash_categoryalbum").hide();	
		}
    });
    $('.submit_trash_categoryalbum').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Categoryalbum"}));
		$form.submit();
	});
 	$('.restore_trash_categoryalbum').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Categoryalbum"}));
		$form.submit();
	});


	//categoryregulation trash
	 $("#selectAll_categoryregulation").change(function(){

        	$('.select_categoryregulation').each(function(){
			$(this).click();
			});

    });
    $('.select_categoryregulation').click(function(){
    	var on=null;
    	$('.select_categoryregulation').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_categoryregulation").show();
			$(".restore_trash_categoryregulation").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_categoryregulation").hide();
			$(".restore_trash_categoryregulation").hide();	
		}
    });
    $('.submit_trash_categoryregulation').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Categoryregulation"}));
		$form.submit();
	});
 	$('.restore_trash_categoryregulation').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Categoryregulation"}));
		$form.submit();
	});


 	//categoryservice trash
	 $("#selectAll_categoryservice").change(function(){

        	$('.select_categoryservice').each(function(){
			$(this).click();
			});

    });
    $('.select_categoryservice').click(function(){
    	var on=null;
    	$('.select_categoryservice').each(function(){
			if($(this).is(':checked')){
				on +=1; 
			}
		});
		if(on!=null){
			$(".submit_trash_categoryservice").show();
			$(".restore_trash_categoryservice").show();			
			on=null;
		}
		else
		{
			$(".submit_trash_categoryservice").hide();
			$(".restore_trash_categoryservice").hide();	
		}
    });
    $('.submit_trash_categoryservice').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('delete_select_entity',{repos: "AppBundle:Categoryservice"}));
		$form.submit();
	});
 	$('.restore_trash_categoryservice').click(function(e){
		e.preventDefault(); 
		$form=$(this).parent().parent().parent().parent();
		$form.attr('action',Routing.generate('restore_select_entity',{repos: "AppBundle:Categoryservice"}));
		$form.submit();
	});


});