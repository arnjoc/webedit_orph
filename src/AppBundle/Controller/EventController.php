<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Event;

/**
 * @Route("/evenement")
 */
class EventController extends Controller
{
    
     /**
     * @Route("/", name="event_index")
     */
    public function indexAction() 
    {
             
        //$em = $this->getDoctrine()->getManager();

        //$galleries = $em->getRepository('AppBundle:Gallery')->findAll();

        return $this->render('pages\gallery.html.twig', array(

            //'gallery' => $gallery
        ));
    }

    /**
     * @Route("/{slug}",name="event_show")
     */
    public function showAction(Request $request, $slug)
    {
        
        $em = $this->getDoctrine()->getManager();
        $event= $em->getRepository('AppBundle:Event')->findOneBySlug($slug);
        
        if (!$event) {
            return $this->redirectToRoute('homepage');
        }

        $recentEvents = $em->getRepository('AppBundle:Event')->relativeEvents($event->getId());
              
        return $this->render('pages\post_show.html.twig', [
            'post' =>$event,
            'recentPosts'   =>$recentEvents
            ]);

    }
}
