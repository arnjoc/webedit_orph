<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Post;
use AppBundle\Entity\ContactUs;
use AppBundle\Form\ContactUsType;

/**
 * @Route("/contactez-nous")
 */
class ContactController extends Controller
{

    /**
     * @Route("/", name="contact_us")
     */
    public function indexAction(Request $request) 
    {
             
        $em = $this->getDoctrine()->getManager();
        $contact = new ContactUs();
        $form = $this->createForm(new ContactUsType(), $contact);
        $siteInfo = $em->getRepository('AppBundle:SiteInfo')->findOneBy([], ['id'=>'desc']);

        $form->handleRequest($request);

        if($form->IsValid() && $form->isSubmitted())
        {
           $em->persist($contact);
           $em->flush();

           $this->addFlash('contact', 'Votre message a bien été envoyé.');

           return $this->redirectToRoute('contact_us');
        }

        return $this->render('pages\contact.html.twig', array(
            'siteInfo' =>$siteInfo,
            'form' =>$form->createView(),
        ));
    }
}
