<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ContactUs;
use AppBundle\Form\ContactUsType;
use AppBundle\Entity\Post;

/**
 * @Route("/a-propos")
 */
class AboutController extends Controller
{
    
     /**
     * @Route("/", name="about")
     */
    public function indexAction(Request $request) 
    {
        /*$em = $this->getDoctrine()->getManager();
        $contact = new ContactUs();
        $form = $this->createForm(new ContactUsType(), $contact);

        $introAbout = $em->getRepository('AppBundle:Post')->oneByCategorySlug('intro-a-propos');
        $introSlider = $em->getRepository('AppBundle:Post')->oneByCategorySlug('a-propos');
        $joinUs = $em->getRepository('AppBundle:Post')->oneByCategorySlug('nous-rejoindre');

        $form->handleRequest($request);

        if($form->IsValid() && $form->isSubmitted())
        {
           $em->persist($contact);
           $em->flush();

           $this->addFlash('contact', 'Votre message a bien été envoyé.');

           return $this->redirectToRoute('about');
        }
*/
        return $this->render('pages\about.html.twig', array(
            /*'introAbout' =>$introAbout,
            'introSlider' =>$introSlider,
            'joinUs' =>$joinUs,
            'form' =>$form->createView()*/
        ));
    }

    /**
     * @Route("/{postSlug}", name="about_post_show")
     * @Method("GET")
     */
    public function postShowAction(Request $request, $postSlug) 
    {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->findOneBySlug($postSlug);
        $relatedPosts = $em->getRepository('AppBundle:Post')->findByCategory($post->getCategory());

        if(!$post) {
            throw $this->createNotFoundException("Aucun article trouvé, veuillez verifier l'url.");
            
        }
        
        return $this->render('default\post-show.html.twig', array(
            'post' => $post,
            'relatedPosts' => $relatedPosts
        
        ));
    }

    public function ourTeamAction() 
    {

        $em = $this->getDoctrine()->getManager();
        $team = $em->getRepository('AppBundle:Post')->oneByCategorySlug('le-staff');
        
        return $this->render('includes\our_team.html.twig', array(
            'team' => $team
        
        ));
    }


    public function joinUsAction(Request $request) 
    {

        $em = $this->getDoctrine()->getManager();
        $contact = new ContactUs();
        $form = $this->createForm(new ContactUsType(), $contact);
        $joinUs = $em->getRepository('AppBundle:Post')->oneByCategorySlug('nous-rejoindre');

        $form->handleRequest($request);

        if($form->IsValid() && $form->isSubmitted())
        {
           $em->persist($contact);
           $em->flush();

           $this->addFlash('success', 'Votre message a bien été envoyé.');

           return $this->redirectToRoute('about');
        }

        return $this->render('includes\join_us.html.twig', array(
            'joinUs' => $joinUs,
            'form' => $form
        ));
    }
}
