<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Donation;
use AppBundle\Form\DonationType;

/**
 * @Route("/faire-un-don")
 */
class DonateController extends Controller
{
    
    /**
     * @Route("/", name="donate")
     */
    public function indexAction() 
    {
             
        /*$em = $this->getDoctrine()->getManager();
        $introDonation = $em->getRepository('AppBundle:Post')->oneByCategorySlug('intro-faire-un-don');
        $donations = $em->getRepository('AppBundle:Post')->byCategorySlug('faire-un-don');
*/
        return $this->render('pages\donate.html.twig', array(
           /*'introDonation' =>$introDonation,
           'donations' =>$donations*/
        ));
    }

    /**
     * @Route("/{postSlug}", name="event_post_show")
     * @Method("GET")
     */
    public function postShowAction(Request $request, $postSlug) 
    {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->findOneBySlug($postSlug);
        $relatedPosts = $em->getRepository('AppBundle:Post')->findByCategory($post->getCategory());

        if(!$post) {
            throw $this->createNotFoundException("Aucun article trouvé, veuillez verifier l'url.");
            
        }
        
        return $this->render('default\post-show.html.twig', array(
            'post' => $post,
            'relatedPosts' => $relatedPosts
        
        ));
    }

    /**
     * Creates a new Donation entity.
     *
     * @Route("/new", name="donation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $donation = new Donation();

        $form = $this->createForm(new DonationType(), $donation);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($donation);
            $em->flush();

            $this->addFlash('donate', $this->get('translator')->trans('Vos informations ont bien été enregistrées, nous vous contacterons'));

            return $this->redirectToRoute('donate');
        }
        //dump('hello'); die();
        return $this->render('pages/donate.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
