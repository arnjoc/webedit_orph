<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ContactUs;
use AppBundle\Entity\SliderText;
use AppBundle\Form\ContactUsType;
use AppBundle\Entity\Donation;
use AppBundle\Form\DonationType;


class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function homeGalleryAction() 
    {
             
        /*$em = $this->getDoctrine()->getManager();
        $info = $em->getRepository('AppBundle:SiteInfo')->findOneBy([], ['id'=>'desc']);
        $banner = $em->getRepository('AppBundle:Post')->oneByCategorySlug('banniere');
        $sliderTexts = $em->getRepository('AppBundle:SliderText')->findAll();
        $partners = $em->getRepository('AppBundle:Partner')->findAll();
        $latestEvents = $em->getRepository('AppBundle:Post')->loadLatestEvents('evenement');
        $latestVideos = $em->getRepository('AppBundle:VideoGallery')->findBy([], ['id'=>'desc'], 3);
        $urls = [];
        if(count($banner) > 0) {

            foreach ($banner->getDossierImages() as $key => $value) {
                $urls[] = $value->getUrl();
            }
        }*/

        return $this->render('pages\index.html.twig', array(
           /*'partners' => $partners,
           'sliderTexts' => $sliderTexts,
           'latestEvents' => $latestEvents,
           'latestVideos' => $latestVideos,
           'info' => $info,
           'urls' => $urls*/
        ));
    }

    public function siteInfosAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $info = $em->getRepository('AppBundle:SiteInfo')->findOneBy([], ['id'=>'desc']);

        return $this->render('includes\site_info.html.twig', array(
            'info' =>$info
        ));
    }

    /*
     * Return socials media links
     */
     public function socialsLinksAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $info = $em->getRepository('AppBundle:SiteInfo')->findOneBy([], ['id'=>'desc']);

        return $this->render('includes\socials-links.html.twig', array(
            'info' =>$info
        ));
    }

    /*
     * Return socials media links
     */
     public function headerSocialsAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $info = $em->getRepository('AppBundle:SiteInfo')->findOneBy([], ['id'=>'desc']);
        //dump($info); die();
        return $this->render('includes\header.html.twig', array(
            'info' =>$info
        ));
    }

    /**
     * Subscribe to the newsletter.
     *
     * @Route("/recherche", name="result_page")
     * @Method({"GET", "POST"})
     */
     public function searchEnginAction() 
    {
       
        return $this->render('pages\search_result_page.html.twig', array(
            //'info' =>$info
        ));
    }

    public function sliderAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $sliders = $em->getRepository('AppBundle:Slider')->findAll();
        //dump($sliders); die();
        return $this->render('includes\main_slider.html.twig', array(
            'sliders' =>$sliders
        ));
    }

    public function eventsAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository('AppBundle:Event')->loadLatestEvents(3);
        //dump($events); die();
        return $this->render('includes\latest_events.html.twig', array(
            'events' =>$events
        ));
    }

    /*
     * Call to action Parallax section 
     */
    public function donatorParallaxHomeAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $callAction = $em->getRepository('AppBundle:CallAction')->paralllaxCategory('acceuil');
        //dump($callAction); die();
        return $this->render('includes\become_donator.html.twig', array(
            'callAction' =>$callAction
        ));
    }

    /*
     * Call to action Parallax section 
     */
    public function parallaxHomeBannerAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $callAction = $em->getRepository('AppBundle:CallAction')->paralllaxCategory('acceuil');
        //dump($callAction->getMedias()[0]['url']); die();
        $urls = [];
        if(count($callAction->getMedias()) > 0) {

            foreach ($callAction->getMedias() as $media) {
                $urls[] = $media->getUrl();
            }
        }
        return $this->render('includes\parallax_banner.html.twig', array(
            'urls' =>$urls
        ));
    }

    /*
     * Call to action Parallax section 
     */
    public function recentActivitiesAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $recentActivities = $em->getRepository('AppBundle:Post')->loadRecentActivities('nos-activites');
        //dump($recentActivities); die();
        return $this->render('includes\recent_activities.html.twig', array(
            'recentActivities' =>$recentActivities
        ));
    }

    /*
     * Call to action Parallax section 
     */
    public function footerInfoAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $siteInfo = $em->getRepository('AppBundle:SiteInfo')->findOneBy([], ['id'=>'desc']);
        $donationTypes = $em->getRepository('AppBundle:Post')->byCategorySlug('faire-un-don');
        
        return $this->render('includes\footer.html.twig', array(
            'siteInfo' => $siteInfo,
            'donationTypes' => $donationTypes
        ));
    }

    /*=====================
        About Page
     ======================*/

    /*
     * Ariane Banner section 
     */
    public function arianeBannerAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $arianeBanner = $em->getRepository('AppBundle:ArianeBanner')->oneByCategorySlug('a-propos');
        //dump($arianeBanner); die();
        return $this->render('includes\ariane_banner.html.twig', array(
            'arianeBanner' =>$arianeBanner
        ));
    }

    /*
     * Ariane Banner section 
     */
    public function ourStoryAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $ourStory = $em->getRepository('AppBundle:Post')->oneByCategorySlug('presentation');
        //dump($arianeBanner); die();
        return $this->render('includes\our_story.html.twig', array(
            'ourStory' =>$ourStory
        ));
    }

    /*
     * Ariane Banner section 
     */
    public function ourGoalAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $ourGoal = $em->getRepository('AppBundle:Post')->oneByCategorySlug('objectif');
        //dump($arianeBanner); die();
        return $this->render('includes\our_goal.html.twig', array(
            'ourGoal' =>$ourGoal
        ));
    }

     /*
     * Call to action Parallax section 
     */
    public function memberParallaxAboutAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $callAction = $em->getRepository('AppBundle:CallAction')->paralllaxCategory('a-propos');
        //dump($callAction); die();
        return $this->render('includes\become_member.html.twig', array(
            'callAction' =>$callAction
        ));
    }

    /*
     * Call to action Parallax section 
     */
    public function parallaxAboutBannerAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $callAction = $em->getRepository('AppBundle:CallAction')->paralllaxCategory('acceuil');
        //dump($callAction->getMedias()[0]['url']); die();
        $urls = [];
        if(count($callAction->getMedias()) > 0) {

            foreach ($callAction->getMedias() as $media) {
                $urls[] = $media->getUrl();
            }
        }
        return $this->render('includes\parallax_banner.html.twig', array(
            'urls' =>$urls
        ));
    }

     /*
     * Call to action Our Team  
     */
    public function ourTeamAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $teams = $em->getRepository('AppBundle:Team')->findAll();
        
        return $this->render('includes\our_team.html.twig', array(
            'teams' =>$teams
        ));
    }


    /*======================
        Our Activities Page
     =======================*/

     /*
     * Call to action Our Team  
     */
    public function ourActivitiesAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $activities = $em->getRepository('AppBundle:Activity')->findAll();
        
        return $this->render('includes\our_activities.html.twig', array(
            'activities' =>$activities
        ));
    }


    /*======================
        Gallery Page
     =======================*/

      /*
      * Gallery  
      */
     public function galleryAction() 
     {
              
         $em = $this->getDoctrine()->getManager();
         $galleries = $em->getRepository('AppBundle:Gallery')->findAll();
         $galleryCategories = $em->getRepository('AppBundle:GalleryCategory')->findAll();
         
         return $this->render('includes\our_gallery.html.twig', array(
             'galleries' =>$galleries,
             'galleryCategories' =>$galleryCategories
         ));
     }

      /*
      * Faire un don  
      */
     public function donateAction() 
     {
              
         $em = $this->getDoctrine()->getManager();
         $donateTexts = $em->getRepository('AppBundle:Post')->byCategorySlug('faire-un-don');
         //dump($donateTexts); die();
         return $this->render('includes\donate_content.html.twig', array(
             'donateTexts' =>$donateTexts,
         ));
     }

       /*
      * Faire un don form Modal
      */
     public function donateModalAction() 
     {
              
        //dump($request); die();
        $donation = new Donation();

        $form = $this->createForm(new DonationType(), $donation);

         return $this->render('includes\donate_modal.html.twig', array(
             'form' =>$form->createView(),
         ));
     }

}
