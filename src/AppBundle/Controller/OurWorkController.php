<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Post;

/**
 * @Route("/nos-activites")
 */
class OurWorkController extends Controller
{
    
    /**
     * @Route("/", name="our_work")
     */
    public function indexAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $ourWorkIntro = $em->getRepository('AppBundle:Post')->oneByCategorySlug('intro-activites');
        $ourWork = $em->getRepository('AppBundle:Post')->byCategorySlug('nos-activites');

        return $this->render('pages\our_activities.html.twig', array(
            'ourWorkIntro'  => $ourWorkIntro,
            'ourWork'       => $ourWork
        ));
    }


    /**
     * @Route("/{postSlug}", name="event_post_show")
     * @Method("GET")
     */
    public function postShowAction(Request $request, $postSlug) 
    {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->findOneBySlug($postSlug);
        $relatedPosts = $em->getRepository('AppBundle:Post')->findByCategory($post->getCategory());

        if(!$post) {
            throw $this->createNotFoundException("Aucun article trouvé, veuillez verifier l'url.");
            
        }
        
        return $this->render('default\post-show.html.twig', array(
            'post' => $post,
            'relatedPosts' => $relatedPosts
        
        ));
    }

    public function ourWorkAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $activities = $em->getRepository('AppBundle:Post')->byCategorySlug('nos-activites');

        return $this->render('includes\our-activities.html.twig', array(
            'activities' =>$activities
        ));
    }


}
