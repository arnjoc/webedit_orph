<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Member;
use AppBundle\Form\MemberType;

/**
 * @Route("/devenir-member")
 */
class MemberController extends Controller
{
    
    /**
     * @Route("/", name="become_member")
     */
    public function indexAction() 
    {
             
        /*$em = $this->getDoctrine()->getManager();
        $introMember = $em->getRepository('AppBundle:Post')->oneByCategorySlug('intro-faire-un-don');
        $members = $em->getRepository('AppBundle:Post')->byCategorySlug('faire-un-don');
*/
        return $this->render('pages\member.html.twig', array(
           /*'introMember' =>$introMember,
           'Members' =>$members*/
        ));
    }

    /**
     * @Route("/{postSlug}", name="become_member_show")
     * @Method("GET")
     */
    public function postShowAction(Request $request, $postSlug) 
    {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->findOneBySlug($postSlug);
        $relatedPosts = $em->getRepository('AppBundle:Post')->findByCategory($post->getCategory());

        if(!$post) {
            throw $this->createNotFoundException("Aucun article trouvé, veuillez verifier l'url.");
            
        }
        
        return $this->render('default\post-show.html.twig', array(
            'post' => $post,
            'relatedPosts' => $relatedPosts
        
        ));
    }

    /**
     * Create a new Member entity.
     *
     * @Route("/new", name="become_member_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $member = new Member();

        $form = $this->createForm(new MemberType(), $member);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($member);
            $em->flush();

            $this->addFlash('member', $this->get('translator')->trans('Vos requête a bien été enregistrée, nous vous contacterons dès quue possible.'));

            return $this->redirectToRoute('homepage');
        }

        
        return $this->render('includes/become_member_modal.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
