<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Post;

/**
 * @Route("/galerie")
 */
class GalleryController extends Controller
{
    
     /**
     * @Route("/", name="gallery")
     */
    public function indexAction() 
    {
             
        //$em = $this->getDoctrine()->getManager();

        //$galleries = $em->getRepository('AppBundle:Gallery')->findAll();

        return $this->render('pages\gallery.html.twig', array(

            //'gallery' => $gallery
        ));
    }

    /**
     * @Route("/categorie/{slug}", name="gallery_category")
     */
    public function galleryCategoryAction($slug) 
    {
             
        /*$em = $this->getDoctrine()->getManager();
        $galleryMenu = $em->getRepository('AppBundle:Post')->byCategorySlug('galerie');
        $currentGallery = $em->getRepository('AppBundle:Post')->findOneBySlug($slug);*/

        return $this->render('pages\gallery.html.twig', array(
            /*'galleryMenu' =>$galleryMenu,
            'currentGallery' =>$currentGallery*/
        ));
    }

    /**
     * @Route("/{slug}",name="gallery_show")
     */
    public function showAction(Request $request, $slug)
    {
        
        $em = $this->getDoctrine()->getManager();
        $gallery= $em->getRepository('AppBundle:Gallery')->findOneBySlug($slug);
        //dump($gallery); die();
        if (!$gallery) {
            return $this->redirectToRoute('homepage');
        }

        $relativeGalleries = $em->getRepository('AppBundle:Gallery')->relativeGalleries($gallery->getId(), $gallery->getGalleryCategory());
         //dump($relativeGalleries); die();     
        return $this->render('pages\gallery_details.html.twig', [
            'gallery' =>$gallery,
            'relativeGalleries'   =>$relativeGalleries
            ]);

    }
}
