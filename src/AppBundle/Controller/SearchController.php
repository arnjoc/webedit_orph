<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Post;
use AppBundle\Entity\Event;
use AppBundle\Form\PostType;
use AppBundle\Form\SearchType;

/**
 * @Route("/search")
* 
*/
class SearchController extends Controller
{
	/* Traitement du formulare de la recherche */

     public function searchAction()
    {
        
        return $this->render('pages\search_result_page.html.twig',[
            //'info' =>$info
        ]);
    }

     /**
     * @Route("/resulte", name="search_engin")
     */
    public function searcTreatmenthAction(Request $request)
    {
        $postResults=[];
        $eventResults=[];
        $search = '';
        if ($request->request->get("_search")) {
           // dump($request->request->get("_search")); die();
           $em = $this->getDoctrine()->getManager();
           $chaine = trim(strip_tags($request->request->get("_search")));
           $posts = $em->getRepository('AppBundle:Post')->findAll();
           $events = $em->getRepository('AppBundle:Event')->findAll();
           foreach ( $posts as $title => $value) {
            if (strpos($value->getTitle(),  $chaine) != 0) {
                  $postResults[]=$value;
               }
            }

            foreach ( $events as $k => $val) {
            if (strpos($val->getTitle(),  $chaine) != 0) {
                  $eventResults[]=$val;
               }
            }
        }
        else
        {
            return $this->redirectToRoute('homepage');
        }

        //dump($postResults, $eventResults); die();

         return $this->render('pages\search_result_page.html.twig', [
                'postResults' =>$postResults,
                'eventResults' =>$eventResults,
                'search' => $chaine
           ]);
    }

}