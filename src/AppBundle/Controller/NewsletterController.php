<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\NlRecipient;
use AppBundle\Form\NlRecipientType;

/**
 * @Route("/newsletter-form")
 */
class NewsletterController extends Controller
{

     /**
     * Subscribe to the newsletter.
     *
     * @Route("/neasletter-form", name="newsletter_form")
     * @Method({"GET", "POST"})
     */
    public function newsletterFormAction(Request $request)
    {
        //dump($request); die();
        $newsletter = new NlRecipient();

        $form = $this->createForm(new NlRecipientType(), $newsletter);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($newsletter);
            $em->flush();

            $this->addFlash('flash_front', $this->get('translator')->trans('Félicitation! Vous avez bien été ajouté.'));

            return $this->redirectToRoute('homepage');
        }

        
        return $this->render('includes/newsletter_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
