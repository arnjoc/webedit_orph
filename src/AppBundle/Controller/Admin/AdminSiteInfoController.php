<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\SiteInfo;
use AppBundle\Form\SiteInfoType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("admin/parametres/infos-site")
 * 
 * @author Arnaud Anato <arnato1@gmail.com>
 */
class AdminSiteInfoController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * @Route("/", name="admin_site_info_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $siteInfo = $em->getRepository('AppBundle:SiteInfo')->findAll();

        return $this->render('admin\index.html.twig',[
            "siteInfo"=> $siteInfo,
        ]);
    }

    /**
     * Create Site Info data
     * 
     * @Route("/new", name="admin_site_info_new")
     */
    public function newAction(Request $request)
    {
        $siteInfo = new SiteInfo();
        $form = $this->createForm( new SiteInfoType(), $siteInfo);

        $form->handleRequest($request);

        if($form->isValid()) {
           //dump($siteInfo); die();
           $em = $this->getDoctrine()->getManager();
           $em->persist($siteInfo);
           $em->flush();

           $this->addFlash('success', 'Les données de l\'adresse ont bien été créées');

           return $this->redirectToRoute('admin_site_info_edit', ['id' => $siteInfo->getId()]);
        }
        
        return $this->render('admin\siteInfo\edit.html.twig',[
            "siteInfo"=> $siteInfo,
            "form"=> $form->createView(),
        ]);
    }

    /**
     * Edit siteInfo data
     * 
     * @Route("/edit/{id}", name="admin_site_info_edit")
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $siteInfo = $em->getRepository('AppBundle:SiteInfo')->find($id);
        $oldSocials = new ArrayCollection();
        $form = $this->createForm( new SiteInfoType(), $siteInfo);

        $form->handleRequest($request);

        if($form->isValid()) {
           //dump($siteInfo); die();
           
           foreach ($oldSocials as $social) {
               if (!$siteInfo->getSocials()->contains($social)) {
                   
                   $social->setSiteInfo(null);

                   $em->remove($social);
               }
           }

           $em->persist($siteInfo);
           $em->flush();

           $this->addFlash('success', 'Les données ont bien été sauvées');

           return $this->redirectToRoute('admin_site_info_edit', ['id' => $siteInfo->getId()]);
        }
        
        return $this->render('admin\siteInfo\edit.html.twig',[
            "siteInfo"=> $siteInfo,
            "form"=> $form->createView(),
        ]);
    }

    /**
     * count siteInfo in DB
     * 
     * @Route("/fr", name="admin_site_info_exist")
     */
    public function checkSiteInfoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $siteInfo = $em->getRepository('AppBundle:SiteInfo')->findAll();

        $new  = $this->generateUrl('admin_site_info_new');

        if(count($siteInfo) > 0) {
            $edit = $this->generateUrl('admin_site_info_edit', ['id' => $siteInfo[0]->getId()]);
            return new Response($edit);
        }
        
        return new Response($new);
    }


}
