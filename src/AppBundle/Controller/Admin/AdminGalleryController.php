<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Gallery;
use AppBundle\Form\GalleryType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("admin/galerie")
 * 
 */
class AdminGalleryController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_gallery_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $galleries = $em->getRepository('AppBundle:Gallery')->findAll();

        return $this->render('admin/gallery/gallery_index.html.twig',[
            'galleries' => $galleries,
        ]);
    }

    /**
     * Creates a new Gallery entity.
     *
     * @Route("/new", name="admin_gallery_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $gallery = new Gallery();

        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($gallery->getMedias() as $media) {
            $oldMedias->add($media);
        }
        
        $form = $this->createForm(new GalleryType(), $gallery);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$gallery->getMedias()->contains($media)) {
                    
                    $media->setGallery(null);

                    $em->remove($media);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($gallery);
            $em->flush();

            $this->addFlash('success', 'les données sauvées avec succès');

            return $this->redirectToRoute('admin_gallery_index');
        }
        
        return $this->render('admin/gallery/gallery_new.html.twig', [
            'form' => $form->createView(),
            'gallery' => $gallery
        ]);
    }

    /**
     * Creates a new evenement entity.
     *
     * @Route("/edit/{id}", name="admin_gallery_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Gallery $gallery)
    {
        if(!$gallery) {
            throw $this->createNotFoundException("Ceci n'existe pas");
        }

        $form = $this->createForm(new GalleryType(), $gallery);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($gallery);
            $em->flush();

            $this->addFlash('success', 'Données mise à jour avec succès');

            return $this->redirectToRoute('admin_gallery_index');
        }
        
        return $this->render('admin/gallery/gallery_new.html.twig', [
            'form' => $form->createView(),
            'gallery' => $gallery
        ]);
    }

    /**
     * Show a evenement entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_gallery_show")
     */
    public function showAction(Gallery $gallery)
    {
        
        return $this->render('admin/gallery/show.html.twig', [
            'gallery'        => $gallery,
        ]);
    }

    /**
     * Delete a Gallery entity.
     *
     * @Route("/{id}/delete", name="admin_gallery_delete")
     */
    public function deleteAction(Gallery $gallery)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($gallery);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('gallery.flash.deleted'));
        return $this->redirectToRoute('admin_gallery_index');
    }


}