<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\CallActionCategory;
use AppBundle\Form\CallActionCategoryType;

/**
 * Controller used to manage post's callActionCategory in admin panel.
 *
 * @Route("admin/categorie-section-parallax")
 *
 */
class AdminCallActionCategoryController extends Controller
{
    /**
     * Lists all callActionCategory entities.
     *
     * @Route("/", name="admin_call_action_category_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $this->getDoctrine()->getRepository('AppBundle:CallActionCategory')->findAll();
        
        return $this->render('admin/call_action_category/category_list.html.twig', ['categories' => $categories,]);
    }

    /**
     * Creates a new callActionCategory entity.
     *
     * @Route("/new", name="admin_call_action_category_new")
     */
    public function newAction(Request $request)
    {
        $callActionCategory = new CallActionCategory();
        $form = $this->createForm(new CallActionCategoryType(), $callActionCategory);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($callActionCategory);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('postcat.flash.created'));

            return $this->redirectToRoute('admin_call_action_category_index');
        }

        return $this->render('admin/call_action_category/new_category.html.twig',
                            ['form' => $form->createView(),]);
    }

    /**
     * Edit an existing callActionCategory entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_call_action_category_edit")
     */
    public function editAction(Request $request, CallActionCategory $callActionCategory)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new CallActionCategoryType(), $callActionCategory);
        //$logs=$this->get('app.loggable')->getList($callActionCategory);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('postcat.flash.updated'));

            return $this->redirectToRoute('admin_call_action_category_index');
        }

        return $this->render('admin/call_action_category/category_edit.html.twig',
                            ['form' => $form->createView(),
                              "id" => $callActionCategory->getId(),
                              "callActionCategory"=>$callActionCategory,
                              //'logs'=>$logs
                            ]);
    }

    /**
     * Delete a callActionCategory entity.
     *
     * @Route("/{id}/delete", name="admin_call_action_category_delete")
     */
    public function deleteAction(CallActionCategory $callActionCategory)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($callActionCategory);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('postcat.flash.deleted'));

        return $this->redirectToRoute('admin_call_action_category_index');
    }

}