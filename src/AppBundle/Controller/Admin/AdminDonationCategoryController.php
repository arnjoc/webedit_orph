<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\DonationCategory;
use AppBundle\Form\DonationCategoryType;

/**
 * Controller used to manage Donation's category in admin panel.
 *
 * @Route("admin/donation_category")
 *
 */
class AdminDonationCategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/", name="admin_donation_category_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $this->getDoctrine()->getRepository('AppBundle:DonationCategory')->findAll();
        
        return $this->render('admin/donation_category/donation_category_index.html.twig', ['categories' => $categories,]);
    }

    /**
     * Creates a new Category entity.
     *
     * @Route("/new", name="admin_donation_category_new")
     */
    public function newAction(Request $request)
    {
        $category = new DonationCategory();
        $form = $this->createForm(new DonationCategoryType(), $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('Donationcat.flash.created'));

            return $this->redirectToRoute('admin_donation_category_index');
        }

        return $this->render('admin/donation_category/donation_category_new.html.twig',
                            ['form' => $form->createView(),]);
    }

    /**
     * Edit an existing Category entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_donation_category_edit")
     */
    public function editAction(Request $request, DonationCategory $category)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new DonationCategoryType(), $category);
        //$logs=$this->get('app.loggable')->getList($category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('Donationcat.flash.updated'));

            return $this->redirectToRoute('admin_donation_category_index');
        }

        return $this->render('admin/donation_category/donation_category_edit.html.twig',
                            ['form' => $form->createView(),
                              "id" => $category->getId(),
                              "category"=>$category,
                              //'logs'=>$logs
                            ]);
    }

    /**
     * Delete a category entity.
     *
     * @Route("/{id}/delete", name="admin_donation_category_delete")
     */
    public function deleteAction(DonationCategory $category)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($category);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('Donationcat.flash.deleted'));

        return $this->redirectToRoute('admin_donation_category_index');
    }

}