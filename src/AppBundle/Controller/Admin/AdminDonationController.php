<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Donation;
use AppBundle\Entity\Translations;
use AppBundle\Form\DonationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Controller used to manage Donation contents in admin panel.
 *
 * @Route("admin/donation")
 *
 * @author Prudence Assogba <jprud67@gmail.com>
 */
class AdminDonationController extends Controller
{
    /**
     * Lists all Donation entities.
     *
     * @Route("/", name="admin_donation_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $donations = $em->getRepository('AppBundle:Donation')->findAll();
        //$donations = $this->get('knp_paginator')->paginate($findDonations, $request->query->getInt('page', 1),5);
        //dump($donations); die();
        return $this->render('admin/donation/donations_index.html.twig',
                            ['donations' => $donations]);
    }

    /**
     * Creates a new Donation entity.
     *
     * @Route("/new", name="admin_donation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $donation = new Donation();
        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($donation->getMedias() as $media) {
            $oldMedias->add($media);
        }

        $form = $this->createForm(new DonationType(), $donation);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$donation->getMedias()->contains($media)) {
                    
                    $media->setDonations(null);

                    $em->remove($media);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($donation);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('donation.flash.created'));

            return $this->redirectToRoute('admin_donation_index');
        }
        //dump('hello'); die();
        return $this->render('admin/donation/new_donation.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show a Donation entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_donation_show")
     */
    public function showAction(Donation $donation)
    {
        
        return $this->render('admin/blog/show.html.twig', [
            'donation'        => $donation,
        ]);
    }

    /**
     * Edit an existing Donation entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_donation_edit")
     * @Method({"GET", "Donation"})
     */
    public function donationEditAction(Donation $donation, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Media objects in the database
        foreach ($donation->getMedias() as $media) {
            $oldMedias->add($media);
        }

        $form = $this->createForm(new DonationType(), $donation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$donation->getMedias()->contains($media)) {
                    
                    $media->setDonations(null);

                    $em->remove($media);
                }
            }

            $em->persist($donation);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('donation.flash.updated'));

            return $this->redirectToRoute('admin_donation_index', ['id' => $donation->getId()]);
        }

        return $this->render('admin/donation/donation_edit.html.twig', [
            'form'   => $form->createView(),
            'id' => $donation->getId(),
            "Donation"=>$donation
        ]);
    }

    /**
     * Delete a Donation entity.
     *
     * @Route("/{id}/delete", name="admin_donation_delete")
     */
    public function deleteAction(Donation $donation)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($donation);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('donation.flash.deleted'));
        return $this->redirectToRoute('admin_donation_index');
    }

    
}
