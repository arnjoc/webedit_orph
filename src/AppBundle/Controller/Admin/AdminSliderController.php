<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Slider;
use AppBundle\Form\SliderType;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Route("admin/slider")
 * 
 */
class AdminSliderController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_slider_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sliders = $em->getRepository('AppBundle:Slider')->findAll();

        return $this->render('admin\slider\slider_index.html.twig',[
            "sliders"=> $sliders,
        ]);
    }

    /**
     * Creates a new Text Slider entity.
     *
     * @Route("/new", name="admin_slider_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $slider = new Slider();

        $oldMedias = new ArrayCollection();
        
        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($slider->getMedias() as $media) {
            $oldMedias->add($media);
        }
        
        $form = $this->createForm(new SliderType(), $slider);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$slider->getMedias()->contains($media)) {
                    
                    $media->setSlider(null);

                    $em->remove($media);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($slider);
            $em->flush();

            $this->addFlash('success', 'Le slider a bien été créé');

            return $this->redirectToRoute('admin_slider_index');
        }
        
        return $this->render('admin/slider/slider_new.html.twig', [
            'form' => $form->createView(),
            'slider' => $slider
        ]);
    }

    /**
     * Creates a new Text Slider entity.
     *
     * @Route("/edit/{id}", name="admin_slider_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Slider $slider)
    {
        if(!$slider) {
            throw $this->createNotFoundException("Ce slider n'existe pas");
        }
        //dump($slider->getMedias()[0]); die();
        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($slider->getMedias() as $media) {
            $oldMedias->add($media);
        }

        $form = $this->createForm(new SliderType(), $slider);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            
            foreach ($oldMedias as $media) {
                if (!$slider->getMedias()->contains($media)) {
                    
                    $media->setSlider(null);

                    $em->remove($media);
                }
            }
            //dump($slider); die();
            $em = $this->getDoctrine()->getManager();
            $em->persist($slider);
            $em->flush();

            $this->addFlash('success', 'Le slider a bien été mise à jour');

            return $this->redirectToRoute('admin_slider_index');
        }
        
        return $this->render('admin/slider/slider_new.html.twig', [
            'form' => $form->createView(),
            'slider' => $slider
        ]);
    }

    /**
     * Show a Slider entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_slider_show")
     */
    public function showAction(Slider $slider)
    {
        
        return $this->render('admin/slider/show.html.twig', [
            'slider'        => $slider,
        ]);
    }

    /**
     * Delete a Slider entity.
     *
     * @Route("/{id}/delete", name="admin_slider_delete")
     */
    public function deleteAction(Slider $slider)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($slider);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('slider.flash.deleted'));
        return $this->redirectToRoute('admin_slider_index');
    }


}