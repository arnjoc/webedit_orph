<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\GalleryCategory;
use AppBundle\Form\GalleryCategoryType;

/**
 * Controller used to manage post's GalleryCategory in admin panel.
 *
 * @Route("admin/categorie-galerie")
 *
 */
class AdminGalleryCategoryController extends Controller
{
    /**
     * Lists all GalleryCategory entities.
     *
     * @Route("/", name="admin_gallery_category_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $this->getDoctrine()->getRepository('AppBundle:GalleryCategory')->findAll();
        
        return $this->render('admin/gallery_category/gallery_category_index.html.twig', ['categories' => $categories,]);
    }

    /**
     * Creates a new Galley Category entity.
     *
     * @Route("/new", name="admin_gallery_category_new")
     */
    public function newAction(Request $request)
    {
        $galleryCategory = new GalleryCategory();
        $form = $this->createForm(new GalleryCategoryType(), $galleryCategory);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($galleryCategory);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('postcat.flash.created'));

            return $this->redirectToRoute('admin_gallery_category_index');
        }

        return $this->render('admin/gallery_category/gallery_category_new.html.twig',
                            ['form' => $form->createView(),]);
    }

    /**
     * Edit an existing GalleryCategory entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_gallery_category_edit")
     */
    public function editAction(Request $request, GalleryCategory $galleryCategory)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new GalleryCategoryType(), $galleryCategory);
        //$logs=$this->get('app.loggable')->getList($category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('postcat.flash.updated'));

            return $this->redirectToRoute('admin_gallery_category_index');
        }

        return $this->render('admin/gallery_category/gallery_category_edit.html.twig',
                            ['form' => $form->createView(),
                              "id" => $galleryCategory->getId(),
                              "galleryCategory"=>$galleryCategory,
                              //'logs'=>$logs
                            ]);
    }

    /**
     * Delete a GalleryCategory entity.
     *
     * @Route("/{id}/delete", name="admin_gallery_category_delete")
     */
    public function deleteAction(GalleryCategory $galleryCategory)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($galleryCategory);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('postcat.flash.deleted'));

        return $this->redirectToRoute('admin_gallery_category_index');
    }

}