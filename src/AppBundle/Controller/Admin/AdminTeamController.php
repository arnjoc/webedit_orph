<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Team;
use AppBundle\Form\TeamType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("admin/equipe")
 * 
 */
class AdminTeamController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_team_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $teams = $em->getRepository('AppBundle:Team')->findAll();

        return $this->render('admin/team/team_index.html.twig',[
            'teams' => $teams,
        ]);
    }

    /**
     * Creates a new Team entity.
     *
     * @Route("/new", name="admin_team_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $team = new Team();

        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($team->getMedias() as $media) {
            $oldMedias->add($media);
        }
        
        $form = $this->createForm(new TeamType(), $team);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$team->getMedias()->contains($media)) {
                    
                    $media->setTeam(null);

                    $em->remove($media);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();

            $this->addFlash('success', 'les données sauvées avec succès');

            return $this->redirectToRoute('admin_team_index');
        }
        
        return $this->render('admin/team/team_new.html.twig', [
            'form' => $form->createView(),
            'team' => $team
        ]);
    }

    /**
     * Creates a new evenement entity.
     *
     * @Route("/edit/{id}", name="admin_team_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Team $team)
    {
        if(!$team) {
            throw $this->createNotFoundException("Ceci n'existe pas");
        }

        $form = $this->createForm(new TeamType(), $team);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();

            $this->addFlash('success', 'Données mise à jour avec succès');

            return $this->redirectToRoute('admin_team_index');
        }
        
        return $this->render('admin/team/team_new.html.twig', [
            'form' => $form->createView(),
            'team' => $team
        ]);
    }

    /**
     * Show a evenement entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_team_show")
     */
    public function showAction(Team $team)
    {
        
        return $this->render('admin/team/show.html.twig', [
            'team'        => $team,
        ]);
    }

    /**
     * Delete a Team entity.
     *
     * @Route("/{id}/delete", name="admin_team_delete")
     */
    public function deleteAction(Team $team)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($team);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('team.flash.deleted'));
        return $this->redirectToRoute('admin_team_index');
    }


}