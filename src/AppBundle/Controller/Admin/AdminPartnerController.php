<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Partner;
use AppBundle\Form\PartnerType;

/**
 * @Route("admin/partner")
 * 
 */
class AdminPartnerController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_partner_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $partners = $em->getRepository('AppBundle:Partner')->findAll();

        return $this->render('admin\partner\index_partner.html.twig',[
            "partners"=> $partners,
        ]);
    }

    /**
     * Creates a new Text Slider entity.
     *
     * @Route("/new", name="admin_partner_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $partner = new Partner();

        $form = $this->createForm(new PartnerType(), $partner);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($partner);
            $em->flush();

            $this->addFlash('success', 'Le partenaire a bien été créé');

            return $this->redirectToRoute('admin_partner_index');
        }
        
        return $this->render('admin/partner/new_partner.html.twig', [
            'form' => $form->createView(),
            'partner' => $partner
        ]);
    }

    /**
     * Creates a new Partner entity.
     *
     * @Route("/edit/{id}", name="admin_partner_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Partner $partner)
    {
        if(!$partner) {
            throw $this->createNotFoundException("Ce slider texte n'existe pas");
        }

        $form = $this->createForm(new PartnerType(), $partner);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($partner);
            $em->flush();

            $this->addFlash('success', 'Le partenaire a bien été mise à jour');

            return $this->redirectToRoute('admin_partner_index');
        }
        
        return $this->render('admin/partner/new_partner.html.twig', [
            'form' => $form->createView(),
            'partner' => $partner
        ]);
    }

    /**
     * Show a Partner entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_partner_show")
     */
    public function showAction(Partner $partner)
    {
        
        return $this->render('admin/partner/show.html.twig', [
            'partner'        => $partner,
        ]);
    }

    /**
     * Delete a partner entity.
     *
     * @Route("/{id}/delete", name="admin_partner_delete")
     */
    public function deleteAction(Partner $partner)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($partner);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('partner supprimé avec succès'));
        return $this->redirectToRoute('admin_partner_index');
    }


}