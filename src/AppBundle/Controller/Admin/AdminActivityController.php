<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Activity;
use AppBundle\Form\ActivityType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("admin/equipe")
 * 
 */
class AdminActivityController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_activity_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $activities = $em->getRepository('AppBundle:Activity')->findAll();

        return $this->render('admin/activity/activity_index.html.twig',[
            'activities' => $activities,
        ]);
    }

    /**
     * Creates a new Activity entity.
     *
     * @Route("/new", name="admin_activity_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $activity = new Activity();

        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($activity->getMedias() as $media) {
            $oldMedias->add($media);
        }
        
        $form = $this->createForm(new ActivityType(), $activity);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$activity->getMedias()->contains($media)) {
                    
                    $media->setActivity(null);

                    $em->remove($media);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($activity);
            $em->flush();

            $this->addFlash('success', 'les données sauvées avec succès');

            return $this->redirectToRoute('admin_activity_index');
        }
        
        return $this->render('admin/activity/activity_new.html.twig', [
            'form' => $form->createView(),
            'activity' => $activity
        ]);
    }

    /**
     * Creates a new evenement entity.
     *
     * @Route("/edit/{id}", name="admin_activity_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Activity $activity)
    {
        if(!$activity) {
            throw $this->createNotFoundException("Ceci n'existe pas");
        }

        $form = $this->createForm(new ActivityType(), $activity);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($activity);
            $em->flush();

            $this->addFlash('success', 'Données mise à jour avec succès');

            return $this->redirectToRoute('admin_activity_index');
        }
        
        return $this->render('admin/activity/activity_new.html.twig', [
            'form' => $form->createView(),
            'activity' => $activity
        ]);
    }

    /**
     * Show a evenement entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_activity_show")
     */
    public function showAction(Activity $activity)
    {
        
        return $this->render('admin/activity/show.html.twig', [
            'activity'        => $activity,
        ]);
    }

    /**
     * Delete a Activity entity.
     *
     * @Route("/{id}/delete", name="admin_activity_delete")
     */
    public function deleteAction(Activity $activity)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($activity);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('activity.flash.deleted'));
        return $this->redirectToRoute('admin_activity_index');
    }


}