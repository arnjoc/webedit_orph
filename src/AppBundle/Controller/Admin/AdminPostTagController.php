<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Tag;
use AppBundle\Form\TagType;

/**
 * Controller used to manage post's tag in admin panel.
 *
 * @Route("admin/tag")
 *
 * @author Prudence Assogba <jprud67@gmail.com>
 */
class AdminPostTagController extends Controller
{
    /**
     * Lists all tag entities.
     *
     * @Route("/", name="admin_post_tag_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $tags = $this->getDoctrine()->getRepository(new Tag())->findAll();
       // $categories = $this->get('knp_paginator')->paginate($findCategories, $request->query->getInt('page', 1),5);
        return $this->render('admin/tag/tag_list.html.twig',
                            ['tags' => $tags]);
    }

    /**
     * Creates a new Tag entity.
     *
     * @Route("/new", name="admin_post_tag_new")
     */
    public function newAction(Request $request)
    {
        $tag = new Tag();
        $form = $this->createForm(new tagType(), $tag);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($tag);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('tag.flash.created'));

            return $this->redirectToRoute('admin_post_tag_index');
        }

        return $this->render('admin/tag/new_tag.html.twig',
                            ['form' => $form->createView(),]);
    }

    /**
     * Edit an existing Tag entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_post_tag_edit")
     */
    public function editAction(Request $request, Tag $tag)
    {
         $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new TagType(), $tag);
        $logs=$this->get('app.loggable')->getList($tag);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('tag.flash.updated'));

            return $this->redirectToRoute('admin_post_tag_index');
        }

        return $this->render('admin/tag/tag_edit.html.twig',
                            ['form' => $form->createView(),
                              "id" => $tag->getId(),
                              "tag"=>$tag,
                              'logs'=>$logs
                            ]);
    }

    /**
     * Delete a tag entity.
     *
     * @Route("/{id}/delete", name="admin_post_tag_delete")
     */
    public function deleteAction(Tag $tag)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($tag);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('tag.flash.deleted'));

        return $this->redirectToRoute('admin_post_tag_index');
    }

}