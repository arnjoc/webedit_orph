<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ArianeBannerCategory;
use AppBundle\Form\ArianeBannerCategoryType;

/**
 * Controller used to manage ArianeBannerCategory in admin panel.
 *
 * @Route("admin/categorie-section-parallax")
 *
 */
class AdminArianeBannerCategoryController extends Controller
{
    /**
     * Lists all ArianeBannerCategory entities.
     *
     * @Route("/", name="admin_ariane_banner_category_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $this->getDoctrine()->getRepository('AppBundle:ArianeBannerCategory')->findAll();
        
        return $this->render('admin/ariane_banner_category/category_list.html.twig', ['categories' => $categories,]);
    }

    /**
     * Creates a new ArianeBannerCategory entity.
     *
     * @Route("/new", name="admin_ariane_banner_category_new")
     */
    public function newAction(Request $request)
    {
        $arianeBannerCategory = new ArianeBannerCategory();
        $form = $this->createForm(new ArianeBannerCategoryType(), $arianeBannerCategory);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($arianeBannerCategory);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('postcat.flash.created'));

            return $this->redirectToRoute('admin_ariane_banner_category_index');
        }

        return $this->render('admin/ariane_banner_category/new_category.html.twig',
                            ['form' => $form->createView(),]);
    }

    /**
     * Edit an existing ArianeBannerCategory entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_ariane_banner_category_edit")
     */
    public function editAction(Request $request, ArianeBannerCategory $arianeBannerCategory)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ArianeBannerCategoryType(), $arianeBannerCategory);
        //$logs=$this->get('app.loggable')->getList($ArianeBannerCategory);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('postcat.flash.updated'));

            return $this->redirectToRoute('admin_ariane_banner_category_index');
        }

        return $this->render('admin/ariane_banner_category/category_edit.html.twig',
                            ['form' => $form->createView(),
                              "id" => $arianeBannerCategory->getId(),
                              "arianeBannerCategory"=>$arianeBannerCategory,
                              //'logs'=>$logs
                            ]);
    }

    /**
     * Delete a ArianeBannerCategory entity.
     *
     * @Route("/{id}/delete", name="admin_ariane_banner_category_delete")
     */
    public function deleteAction(ArianeBannerCategory $arianeBannerCategory)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($arianeBannerCategory);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('postcat.flash.deleted'));

        return $this->redirectToRoute('admin_ariane_banner_category_index');
    }

}