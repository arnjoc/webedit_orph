<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\VideoGallery;
use AppBundle\Form\VideoGalleryType;

/**
 * @Route("admin/galerie-video")
 * 
 */
class AdminVideoGalleryController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_video_gallery_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $videoGalleries = $em->getRepository('AppBundle:VideoGallery')->findAll();

        return $this->render('admin/gallery/video/index_video.html.twig',[
            "videoGalleries"=> $videoGalleries,
        ]);
    }

    /**
     * Creates a new Video gallery entity.
     *
     * @Route("/new", name="admin_video_gallery_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $videoGallery = new VideoGallery();

        $form = $this->createForm(new VideoGalleryType(), $videoGallery);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($videoGallery);
            $em->flush();

            $this->addFlash('success', 'La video a bien été créé');

            return $this->redirectToRoute('admin_video_gallery_index');
        }
        
        return $this->render('admin/gallery/video/new_video.html.twig', [
            'form' => $form->createView(),
            'videoGallery' => $videoGallery
        ]);
    }

    /**
     * Creates a new Video entity.
     *
     * @Route("/edit/{id}", name="admin_video_gallery_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, VideoGallery $videoGallery)
    {
        if(!$videoGallery) {
            throw $this->createNotFoundException("Cette video n'existe pas");
        }

        $form = $this->createForm(new VideoGalleryType(), $videoGallery);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($videoGallery);
            $em->flush();

            $this->addFlash('success', 'La video a bien été mise à jour');

            return $this->redirectToRoute('admin_video_gallery_index');
        }
        
        return $this->render('admin/videoGallery/new_video_gallery.html.twig', [
            'form' => $form->createView(),
            'videoGallery' => $videoGallery
        ]);
    }

    /**
     * Show a Video entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_video_gallery_show")
     */
    public function showAction(videoGallery $videoGallery)
    {
        
        return $this->render('admin/videoGallery/show.html.twig', [
            'videoGallery'        => $videoGallery,
        ]);
    }

    /**
     * Delete a videoGallery entity.
     *
     * @Route("/{id}/delete", name="admin_video_gallery_delete")
     */
    public function deleteAction(VideoGallery $videoGallery)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($videoGallery);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('videoGallery.flash.deleted'));
        return $this->redirectToRoute('admin_video_gallery_index');
    }


}