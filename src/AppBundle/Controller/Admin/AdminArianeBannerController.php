<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\ArianeBanner;
use AppBundle\Form\ArianeBannerType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("admin/ariane-banner")
 * 
 */
class AdminArianeBannerController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_ariane_banner_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $arianeBanners = $em->getRepository('AppBundle:ArianeBanner')->findAll();

        return $this->render('admin/ariane_banner/ariane_banner_index.html.twig',[
            'arianeBanners' => $arianeBanners,
        ]);
    }

    /**
     * Creates a new ArianeBanner entity.
     *
     * @Route("/new", name="admin_ariane_banner_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $arianeBanner = new ArianeBanner();

        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($arianeBanner->getMedias() as $media) {
            $oldMedias->add($media);
        }
        
        $form = $this->createForm(new ArianeBannerType(), $arianeBanner);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$arianeBanner->getMedias()->contains($media)) {
                    
                    $media->setArianeBanners(null);

                    $em->remove($media);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($arianeBanner);
            $em->flush();

            $this->addFlash('success', 'les données sauvées avec succès');

            return $this->redirectToRoute('admin_ariane_banner_index');
        }
        
        return $this->render('admin/ariane_banner/ariane_banner_new.html.twig', [
            'form' => $form->createView(),
            'arianeBanner' => $arianeBanner
        ]);
    }

    /**
     * Creates a new evenement entity.
     *
     * @Route("/edit/{id}", name="admin_ariane_banner_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ArianeBanner $arianeBanner)
    {
        if(!$arianeBanner) {
            throw $this->createNotFoundException("Ceci n'existe pas");
        }

        $form = $this->createForm(new ArianeBannerType(), $arianeBanner);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($arianeBanner);
            $em->flush();

            $this->addFlash('success', 'Données mise à jour avec succès');

            return $this->redirectToRoute('admin_ariane_banner_index');
        }
        
        return $this->render('admin/ariane_banner/ariane_banner_new.html.twig', [
            'form' => $form->createView(),
            'arianeBanner' => $arianeBanner
        ]);
    }

    /**
     * Show a evenement entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_ariane_banner_show")
     */
    public function showAction(ArianeBanner $arianeBanner)
    {
        
        return $this->render('admin/ariane_banner/show.html.twig', [
            'arianeBanner'        => $arianeBanner,
        ]);
    }

    /**
     * Delete a ArianeBanner entity.
     *
     * @Route("/{id}/delete", name="admin_ariane_banner_delete")
     */
    public function deleteAction(ArianeBanner $arianeBanner)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($arianeBanner);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('arianeBanner.flash.deleted'));
        return $this->redirectToRoute('admin_ariane_banner_index');
    }


}