<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\CallAction;
use AppBundle\Form\CallActionType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("admin/parallax-section")
 * 
 */
class AdminCallActionController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_call_action_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $callActions = $em->getRepository('AppBundle:CallAction')->findAll();

        return $this->render('admin/call_action/call_action_index.html.twig',[
            'callActions' => $callActions,
        ]);
    }

    /**
     * Creates a new callAction entity.
     *
     * @Route("/new", name="admin_call_action_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $callAction = new callAction();

        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($callAction->getMedias() as $media) {
            $oldMedias->add($media);
        }
        
        $form = $this->createForm(new CallActionType(), $callAction);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$callAction->getMedias()->contains($media)) {
                    
                    $media->setcallActions(null);

                    $em->remove($media);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($callAction);
            $em->flush();

            $this->addFlash('success', 'les données sauvées avec succès');

            return $this->redirectToRoute('admin_call_action_index');
        }
        
        return $this->render('admin/call_action/call_action_new.html.twig', [
            'form' => $form->createView(),
            'callAction' => $callAction
        ]);
    }

    /**
     * Creates a new evenement entity.
     *
     * @Route("/edit/{id}", name="admin_call_action_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CallAction $callAction)
    {
        if(!$callAction) {
            throw $this->createNotFoundException("Ceci n'existe pas");
        }

        $form = $this->createForm(new CallActionType(), $callAction);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($callAction);
            $em->flush();

            $this->addFlash('success', 'Données mise à jour avec succès');

            return $this->redirectToRoute('admin_call_action_index');
        }
        
        return $this->render('admin/call_action/call_action_new.html.twig', [
            'form' => $form->createView(),
            'callAction' => $callAction
        ]);
    }

    /**
     * Show a evenement entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_call_action_show")
     */
    public function showAction(callAction $callAction)
    {
        
        return $this->render('admin/call_action/show.html.twig', [
            'callAction'        => $callAction,
        ]);
    }

    /**
     * Delete a callAction entity.
     *
     * @Route("/{id}/delete", name="admin_call_action_delete")
     */
    public function deleteAction(CallAction $callAction)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($callAction);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('callAction.flash.deleted'));
        return $this->redirectToRoute('admin_call_action_index');
    }


}