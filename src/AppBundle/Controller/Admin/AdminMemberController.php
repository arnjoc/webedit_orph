<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Member;
use AppBundle\Form\MemberType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("admin/equipe")
 * 
 */
class AdminMemberController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_member_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $activities = $em->getRepository('AppBundle:Member')->findAll();

        return $this->render('admin/Member/Member_index.html.twig',[
            'activities' => $activities,
        ]);
    }

    /**
     * Creates a new Member entity.
     *
     * @Route("/new", name="admin_member_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $member = new Member();

        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($member->getMedias() as $media) {
            $oldMedias->add($media);
        }
        
        $form = $this->createForm(new MemberType(), $member);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$member->getMedias()->contains($media)) {
                    
                    $media->setMember(null);

                    $em->remove($media);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($member);
            $em->flush();

            $this->addFlash('success', 'les données sauvées avec succès');

            return $this->redirectToRoute('admin_member_index');
        }
        
        return $this->render('admin/Member/Member_new.html.twig', [
            'form' => $form->createView(),
            'Member' => $member
        ]);
    }

    /**
     * Creates a new evenement entity.
     *
     * @Route("/edit/{id}", name="admin_member_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Member $member)
    {
        if(!$member) {
            throw $this->createNotFoundException("Ceci n'existe pas");
        }

        $form = $this->createForm(new MemberType(), $member);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($member);
            $em->flush();

            $this->addFlash('success', 'Données mise à jour avec succès');

            return $this->redirectToRoute('admin_member_index');
        }
        
        return $this->render('admin/Member/Member_new.html.twig', [
            'form' => $form->createView(),
            'Member' => $member
        ]);
    }

    /**
     * Show a evenement entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_member_show")
     */
    public function showAction(Member $member)
    {
        
        return $this->render('admin/Member/show.html.twig', [
            'Member'        => $member,
        ]);
    }

    /**
     * Delete a Member entity.
     *
     * @Route("/{id}/delete", name="admin_member_delete")
     */
    public function deleteAction(Member $member)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($member);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('Member.flash.deleted'));
        return $this->redirectToRoute('admin_member_index');
    }


}