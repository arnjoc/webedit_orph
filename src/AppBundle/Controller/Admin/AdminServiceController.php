<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Service;
use AppBundle\Form\ServiceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller used to manage service contents in admin panel.
 *
 * @Route("admin/service")
 *
 * @author Prudence Assogba <jprud67@gmail.com>
 */
class AdminServiceController extends Controller
{
    /**
     * Lists all service entities.
     *
     * @Route("/", name="admin_service_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('AppBundle:Service')->findAll();
        //$services = $this->get('knp_paginator')->paginate($findservices,$request->query->get('page', 1),5); 
        return $this->render('admin/service/service_index.html.twig',
                            ['services' => $services]);
    }

    /**
     * Creates a new service entity.
     *
     * @Route("/new", name="admin_service_new")
     * @Method({"GET", "post"})
     */
    public function newAction(Request $request)
    {
        $service = new Service();

        $form = $this->createForm(new ServiceType(), $service);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $service->setSlug($this->get('slugger')->slugify($service->getTitle()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($service);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('service.flash.created'));

            return $this->redirectToRoute('admin_service_index');
        }

        return $this->render('admin/service/new_service.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Edit an existing service entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_service_edit")
     * @Method({"GET", "post"})
     */
    public function serviceEditAction(Service $service, Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new ServiceType(), $service);
        $logs=$this->get('app.loggable')->getList($service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('service.flash.updated'));

            return $this->redirectToRoute('admin_service_index', ['id' => $service->getId()]);
        }

        return $this->render('admin/service/service_edit.html.twig', [
            'form'   => $form->createView(),
            'id' => $service->getId(),
            "service"=>$service,
            'logs'=>$logs
        ]);
    }

    /**
     * Delete a service entity.
     *
     * @Route("/{id}/delete", name="admin_service_delete")
     */
    public function deleteAction(Service $service)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($service);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('service.flash.deleted'));
        return $this->redirectToRoute('admin_service_index');
    }

    
}
