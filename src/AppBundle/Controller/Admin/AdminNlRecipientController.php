<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\NlRecipient;
use AppBundle\Form\NlRecipientType;

/**
 * @Route("admin/newsletter")
 * 
 */
class AdminNlRecipientController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_newsletter_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $newsletters = $em->getRepository('AppBundle:NlRecipient')->findAll();

        return $this->render('admin/newsletter/newsletter_index.html.twig',[
            'newsletters' => $newsletters,
        ]);
    }

    /**
     * Delete a Newsletter subscriber.
     *
     * @Route("/{id}/delete", name="admin_newsletter_delete")
     */
    public function deleteAction(NlRecipient $nlRecipient)
    {
        if(!$nlRecipient) {
            throw $this->createNotFoundException("Ce abonnez n'existe pas");  
        }

        $em = $this->getDoctrine()->getManager();

        $email = $nlRecipient->getEmail();

        $em->remove($nlRecipient);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans(sprintf('L\'abonnez %s a  bien été supprimé', $email)));

        return $this->redirectToRoute('admin_newsletter_index');
    }


}