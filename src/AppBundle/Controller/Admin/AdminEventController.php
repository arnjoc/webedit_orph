<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Event;
use AppBundle\Form\EventType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("admin/evenements")
 * 
 */
class AdminEventController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * 
     * @Route("/", name="admin_event_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('AppBundle:Event')->findAll();

        return $this->render('admin/event/event_index.html.twig',[
            'events' => $events,
        ]);
    }

    /**
     * Creates a new Event entity.
     *
     * @Route("/new", name="admin_event_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $event = new Event();

        $oldMedias = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($event->getMedias() as $media) {
            $oldMedias->add($media);
        }
        
        $form = $this->createForm(new EventType(), $event);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldMedias as $media) {
                if (!$event->getMedias()->contains($media)) {
                    
                    $media->setEvents(null);

                    $em->remove($media);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            $this->addFlash('success', 'L\'evenement a bien été ajouté');

            return $this->redirectToRoute('admin_event_index');
        }
        
        return $this->render('admin/event/new_event.html.twig', [
            'form' => $form->createView(),
            'event' => $event
        ]);
    }

    /**
     * Creates a new evenement entity.
     *
     * @Route("/edit/{id}", name="admin_event_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Event $event)
    {
        if(!$event) {
            throw $this->createNotFoundException("Cet evenement n'existe pas");
        }

        $form = $this->createForm(new EventType(), $event);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($event);
            $em->flush();

            $this->addFlash('success', 'L\'évènement a bien été mise à jour');

            return $this->redirectToRoute('admin_event_index');
        }
        
        return $this->render('admin/event/new_event.html.twig', [
            'form' => $form->createView(),
            'event' => $event
        ]);
    }

    /**
     * Show a evenement entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_event_show")
     */
    public function showAction(Event $event)
    {
        
        return $this->render('admin/event/show.html.twig', [
            'event'        => $event,
        ]);
    }

    /**
     * Delete a event entity.
     *
     * @Route("/{id}/delete", name="admin_event_delete")
     */
    public function deleteAction(Event $event)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($event);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('event.flash.deleted'));
        return $this->redirectToRoute('admin_event_index');
    }


}