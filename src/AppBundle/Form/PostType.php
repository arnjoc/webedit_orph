<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\ImageType;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Intl;

class PostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', 'text', [
                'attr' => ['autofocus' => true],
                'label' => 'label.title',
                ])
           /* ->add('locale', ChoiceType::class, [
                         'choices' => array(
                'English' => 'en',
                'frensh' => 'fr',
            ),
            "mapped"=>false,
            ])*/
             ->add('category', 'entity',[
                'class' => 'AppBundle:Category',
                'choice_label' => 'name',
                'label' => 'label.category',
                'required' => false,
                'placeholder' => 'category.placeholder',
                'empty_data'  => null,
                ])
              /*->add('tags', EntityType::class,[
                'class' => 'AppBundle\Entity\Tag',
                'choice_label' => 'name',
                'label' => 'label.tag',
                'required' => false,
                'multiple'=> true,
                'placeholder' => 'tag.placeholder',
                'empty_data'  => null,
                ])*/
            ->add('content', 'ckeditor', [
                'label' => 'label.content',
                'config_name' => 'simple',
                ])
            ->add('medias', 'collection', [
                    'entry_type' => new MediaType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                    'constraints' => new Valid(),
                    'empty_data'  => null
                   ])
            ->add('state', 'choice', array(
                'label'=>'label.state',
                'choices' => array(
                'post.state.production' => "publier",
                'post.state.bubble' => "brouillon",
                ),'preferred_choices' => array('publier'),
                ))
           ->add('publishedAt', 'datetime',[
                'label' => 'label.publishedAt',
               'widget' => 'single_text',
               'attr' => ['class' => 'datepicker'],
               'auto_initialize'=> "now"
                ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
        ->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Post',

        ))
        ;
    }
}
