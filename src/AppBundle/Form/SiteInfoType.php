<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class SiteInfoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                 ->add('title')
                 ->add('description', 'ckeditor', [
                     'label' => 'Description du site',
                     'config_name' => 'simple',
                     ])
                 ->add('location', null, [
                        'label'=>'Adresse',
                    ])
                 ->add('telephone')
                 ->add('mobile')
                 ->add('email')
                 ->add('contactFormText', null, [
                     'label' => 'Texte Newsletter',
                     ])
                 ->add('socials', 'collection', [
                    'entry_type' => new SocialType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                    'constraints' => new Valid(),
                    'empty_data'  => null
                   ])
                 ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SiteInfo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_siteInfo';
    }


}
