<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\ImageType;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Intl;

class DonationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('amount', 'text', [
                'attr' => ['autofocus' => true],
                'label' => 'Montant',
                'attr' => ['placeholder'=>'Montant'],
                ])

             ->add('category', 'entity',[
                'class' => 'AppBundle:DonationCategory',
                'choice_label' => 'name',
                'label' => 'Type de Don',
                'required' => false,
                'placeholder' => 'Type de Don',
                'empty_data'  => null,
                ])

             ->add('name', 'text', [
                'label' => 'Prénom',
                'required' => false,
                ])

             ->add('lastname', 'text', [
                'label' => 'Nom',
                'required' => false,
                ])

             ->add('email', 'text', [
                'label' => 'Email',
                'required' => false,
                ])

             ->add('phone', 'text', [
                'label' => 'téléphone',
                'required' => false,
                ])

             ->add('address', 'text', [
                'label' => 'Adresse',
                'required' => false,
                ])

             ->add('note', 'text', [
                'label' => 'Note additionelle',
                'required' => false,
                ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
        ->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Donation',

        ))
        ;
    }
}
