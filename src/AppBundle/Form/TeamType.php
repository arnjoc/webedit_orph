<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\ImageType;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Intl;

class TeamType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('firstname', 'text', [
                'attr' => ['autofocus' => true],
                'label' => 'Prénom',
                ])
            ->add('lastname', 'text', [
                'attr' => ['autofocus' => true],
                'label' => 'Nom',
                ])
            ->add('medias', 'collection', [
                    'entry_type' => new MediaType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                    'constraints' => new Valid(),
                    'empty_data'  => null
                ])
            ->add('socials', 'collection', [
                    'entry_type' => new SocialType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                    'constraints' => new Valid(),
                    'empty_data'  => null
                   ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
        ->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Team',

        ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'team';
    }
}
