<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\ImageType;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Intl;

class CallActionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', 'text', [
                'attr' => ['autofocus' => true],
                'label' => 'label.title',
                ])

            ->add('callActionCategory', 'entity',[
                'class' => 'AppBundle:CallActionCategory',
                'choice_label' => 'name',
                'label' => 'label.category',
                'required' => false,
                'placeholder' => 'category.placeholder',
                'empty_data'  => null,
                ])

            ->add('content', 'ckeditor', [
                'label' => 'label.content',
                'config_name' => 'simple',
                ])

            ->add('firstBtn', 'text', [
                'label' => 'Text premier bouton',
                'required' => false,
                ])
            ->add('secondBtn', 'text', [
                'label' => 'Text second bouton',
                'required' => false,
                ])
            ->add('medias', 'collection', [
                    'entry_type' => new MediaType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                    'constraints' => new Valid(),
                    'empty_data'  => null
                ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
        ->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CallAction',

        ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'call_action';
    }
}
