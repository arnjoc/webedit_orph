<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CallAction
 *
 * @ORM\Entity
 * @UniqueEntity("title", message="message.title.existe")
 * @ORM\Table(name="call_action")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CallActionRepository")
 * @ORM\HasLifecycleCallbacks
 */
class CallAction 
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"title"})
     * 
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank(message = "le contenu ne doit pas etre vide")
     * @Assert\Length(min = "10", minMessage = "le contenu est trop court")
     */
    private $content;

    /**
     * @ORM\Column(name="createdAt", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
    * @var String
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $firstBtn;

    /**
    * @var String
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $secondBtn;

    /**
     * @ORM\OneToMany(targetEntity="Media", mappedBy="callAction", cascade={"persist"})
     */
    private $medias;

    /**
     * @ORM\ManyToOne(targetEntity="CallActionCategory",
     *                inversedBy="callAction")
     */
    private $callActionCategory;


    /**
     * @ORM\ManyToOne(targetEntity="User",
     *                inversedBy="events")
     */
    private $author;

    public function __construct()
    {
       $this->medias = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Event
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Event
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Event
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Event
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add media
     *
     * @param \AppBundle\Entity\Media $media
     *
     * @return Event
     */
    public function addMedia(\AppBundle\Entity\Media $media)
    {
        $media->setCallAction($this);

        $this->medias[] = $media;

        return $this;
    }

    /**
     * Remove media
     *
     * @param \AppBundle\Entity\Media $media
     */
    public function removeMedia(\AppBundle\Entity\Media $media)
    {
        $this->medias->removeElement($media);
    }

    /**
     * Get media
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedias()
    {
        return $this->medias;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     * @return Event
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set firstBtn
     *
     * @param string $firstBtn
     * @return CallAction
     */
    public function setFirstBtn($firstBtn)
    {
        $this->firstBtn = $firstBtn;

        return $this;
    }

    /**
     * Get firstBtn
     *
     * @return string 
     */
    public function getFirstBtn()
    {
        return $this->firstBtn;
    }

    /**
     * Set secondBtn
     *
     * @param string $secondBtn
     * @return CallAction
     */
    public function setSecondBtn($secondBtn)
    {
        $this->secondBtn = $secondBtn;

        return $this;
    }

    /**
     * Get secondBtn
     *
     * @return string 
     */
    public function getSecondBtn()
    {
        return $this->secondBtn;
    }

    /**
     * Set callActionCategory
     *
     * @param \AppBundle\Entity\CallActionCategory $callActionCategory
     * @return CallAction
     */
    public function setCallActionCategory(\AppBundle\Entity\CallActionCategory $callActionCategory = null)
    {
        $this->callActionCategory = $callActionCategory;

        return $this;
    }

    /**
     * Get callActionCategory
     *
     * @return \AppBundle\Entity\CallActionCategory 
     */
    public function getCallActionCategory()
    {
        return $this->callActionCategory;
    }
}
