<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;


 
/**
 * ArianeBannerCategory
 * 
 * @ORM\Entity
 * @UniqueEntity("name")
 * @ORM\Table(name="ariane_banner_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArianeBannerCategoryRepository")
 */
class ArianeBannerCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="ArianeBanner",
     *                mappedBy="arianeBannerCategory")
     */
    private $arianeBanners;
    
    /**
     * @ORM\Column(name="createdAt", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->arianeBanners = new ArrayCollection();
    }
 

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Category
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Category
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }


    /**
     * Add arianeBanners
     *
     * @param \AppBundle\Entity\ArianeBanner $arianeBanners
     * @return ArianeBannerCategory
     */
    public function addArianeBanner(\AppBundle\Entity\ArianeBanner $arianeBanners)
    {
        $this->arianeBanners[] = $arianeBanners;

        return $this;
    }

    /**
     * Remove arianeBanners
     *
     * @param \AppBundle\Entity\ArianeBanner $arianeBanners
     */
    public function removeArianeBanner(\AppBundle\Entity\ArianeBanner $arianeBanners)
    {
        $this->arianeBanners->removeElement($arianeBanners);
    }

    /**
     * Get arianeBanners
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArianeBanners()
    {
        return $this->arianeBanners;
    }
}
