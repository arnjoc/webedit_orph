<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SiteInfo
 *
 * @ORM\Table(name="site_info")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SiteInfoRepository")
 */
class SiteInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column( type="text", nullable=true)
     */
    private $description;

     /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @var int
     *
     * @ORM\Column(name="telephone", type="string", length=25, nullable=true)
     */
    private $telephone;

    /**
     * @var int
     *
     * @ORM\Column(name="mobile", type="string", length=25, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var text
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $contactFormText;

    /**
     * @ORM\OneToMany(targetEntity="Social",
     *                mappedBy="siteInfo",
     *                orphanRemoval=true,
     *                cascade={"persist"}
     *)
     */
    private $socials;

    function __construct() 
    {
        $this->socials = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return SiteInfo
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return SiteInfo
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return SiteInfo
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     * @return SiteInfo
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string 
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return SiteInfo
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set contactFormText
     *
     * @param string $contactFormText
     * @return SiteInfo
     */
    public function setContactFormText($contactFormText)
    {
        $this->contactFormText = $contactFormText;

        return $this;
    }

    /**
     * Get contactFormText
     *
     * @return string 
     */
    public function getContactFormText()
    {
        return $this->contactFormText;
    }

    /**
     * Add socials
     *
     * @param \AppBundle\Entity\Social $socials
     * @return SiteInfo
     */
    public function addSocial(\AppBundle\Entity\Social $social)
    {
        $social->setSiteInfo($this);

        $this->socials[] = $social;

        return $this;
    }

    /**
     * Remove socials
     *
     * @param \AppBundle\Entity\Social $socials
     */
    public function removeSocial(\AppBundle\Entity\Social $socials)
    {
        $this->socials->removeElement($socials);
    }

    /**
     * Get socials
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSocials()
    {
        return $this->socials;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SiteInfo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
