<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ArianeBanner
 *
 * @ORM\Entity
 * @ORM\Table(name="ariane_anner")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArianeBannerRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ArianeBanner 
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @ORM\Column(name="createdAt", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;
    /**
     * @ORM\OneToMany(targetEntity="Media", mappedBy="arianeBanner", cascade={"persist"})
     */
    private $medias;

    /**
     * @ORM\ManyToOne(targetEntity="ArianeBannerCategory",
     *                inversedBy="arianeBanners")
     */
    private $arianeBannerCategory;


    /**
     * @ORM\ManyToOne(targetEntity="User",
     *                inversedBy="events")
     */
    private $author;

    public function __construct()
    {
       $this->medias = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

  
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Event
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Event
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add media
     *
     * @param \AppBundle\Entity\Media $media
     *
     * @return Event
     */
    public function addMedia(\AppBundle\Entity\Media $media)
    {
        $media->setArianeBanner($this);

        $this->medias[] = $media;

        return $this;
    }

    /**
     * Remove media
     *
     * @param \AppBundle\Entity\Media $media
     */
    public function removeMedia(\AppBundle\Entity\Media $media)
    {
        $this->medias->removeElement($media);
    }

    /**
     * Get media
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedias()
    {
        return $this->medias;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     * @return Event
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }

  
    /**
     * Set ArianeBannerCategory
     *
     * @param \AppBundle\Entity\ArianeBannerCategory $ArianeBannerCategory
     * @return ArianeBanner
     */
    public function setArianeBannerCategory(\AppBundle\Entity\ArianeBannerCategory $arianeBannerCategory = null)
    {
        $this->arianeBannerCategory = $arianeBannerCategory;

        return $this;
    }

    /**
     * Get ArianeBannerCategory
     *
     * @return \AppBundle\Entity\ArianeBannerCategory 
     */
    public function getArianeBannerCategory()
    {
        return $this->arianeBannerCategory;
    }
}
