<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Social
 *
 * @ORM\Table(name="social")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SocialRepository")
 */
class Social
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $iconCode;

    /**
     * @ORM\ManyToOne(targetEntity="SiteInfo",
     *                inversedBy="socials"
     *)
     */
    private $siteInfo;

    /**
     * @ORM\ManyToOne(targetEntity="Team",
     *                inversedBy="socials"
     *)
     */
    private $team;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Social
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Social
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set iconCode
     *
     * @param string $iconCode
     *
     * @return Social
     */
    public function setIconCode($iconCode)
    {
        $this->iconCode = $iconCode;

        return $this;
    }

    /**
     * Get iconCode
     *
     * @return string
     */
    public function getIconCode()
    {
        return $this->iconCode;
    }

    /**
     * Set siteInfo
     *
     * @param \AppBundle\Entity\SiteInfo $siteInfo
     * @return Social
     */
    public function setSiteInfo(\AppBundle\Entity\SiteInfo $siteInfo = null)
    {
        $this->siteInfo = $siteInfo;

        return $this;
    }

    /**
     * Get siteInfo
     *
     * @return \AppBundle\Entity\SiteInfo 
     */
    public function getSiteInfo()
    {
        return $this->siteInfo;
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     * @return Social
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }
}
