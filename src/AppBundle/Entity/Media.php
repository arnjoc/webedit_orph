<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Media
 *
 * @ORM\Table(name="media")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MediaRepository")
 */
class Media
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, unique=false)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="medias")
     */
    private $posts;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Slider", inversedBy="medias")
     */
    private $slider;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="medias")
     */
    private $event;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="CallAction", inversedBy="medias")
     */
    private $callAction;
    
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="ArianeBanner", inversedBy="medias")
     */
    private $arianeBanner;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="medias")
     */
    private $team;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Activity", inversedBy="medias")
     */
    private $activity;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="medias")
     */
    private $gallery;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        // new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Media
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return Media
     
    public function addPost(\AppBundle\Entity\Post $post)
    {
        
        $this->posts[] = $post;

        return $this;
    }*/

    /**
     * Remove post
     *
     * @param \AppBundle\Entity\Post $post
     
    public function removePost(\AppBundle\Entity\Post $post)
    {
        $this->posts->removeElement($post);
    }*/

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Set posts
     *
     * @param \AppBundle\Entity\Post $posts
     *
     * @return Media
     */
    public function setPosts(\AppBundle\Entity\Post $posts = null)
    {
        $this->posts = $posts;

        return $this;
    }

    /**
     * Set siteInfo
     *
     * @param \AppBundle\Entity\SiteInfo $siteInfo
     * @return Media
     */
    public function setSiteInfo(\AppBundle\Entity\SiteInfo $siteInfo = null)
    {
        $this->siteInfo = $siteInfo;

        return $this;
    }

    /**
     * Get siteInfo
     *
     * @return \AppBundle\Entity\SiteInfo 
     */
    public function getSiteInfo()
    {
        return $this->siteInfo;
    }

    /**
     * Set slider
     *
     * @param \AppBundle\Entity\Slider $slider
     * @return Media
     */
    public function setSlider(\AppBundle\Entity\Slider $slider = null)
    {
        $this->slider = $slider;

        return $this;
    }

    /**
     * Get slider
     *
     * @return \AppBundle\Entity\Slider 
     */
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     * Set event
     *
     * @param \AppBundle\Entity\Event $event
     * @return Media
     */
    public function setEvent(\AppBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \AppBundle\Entity\Event 
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set callAction
     *
     * @param \AppBundle\Entity\CallAction $callAction
     * @return Media
     */
    public function setCallAction(\AppBundle\Entity\CallAction $callAction = null)
    {
        $this->callAction = $callAction;

        return $this;
    }

    /**
     * Get callAction
     *
     * @return \AppBundle\Entity\CallAction 
     */
    public function getCallAction()
    {
        return $this->callAction;
    }

    /**
     * Set arianeBanner
     *
     * @param \AppBundle\Entity\ArianeBanner $arianeBanner
     * @return Media
     */
    public function setArianeBanner(\AppBundle\Entity\ArianeBanner $arianeBanner = null)
    {
        $this->arianeBanner = $arianeBanner;

        return $this;
    }

    /**
     * Get arianeBanner
     *
     * @return \AppBundle\Entity\ArianeBanner 
     */
    public function getArianeBanner()
    {
        return $this->arianeBanner;
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     * @return Media
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set activity
     *
     * @param \AppBundle\Entity\Activity $activity
     * @return Media
     */
    public function setActivity(\AppBundle\Entity\Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \AppBundle\Entity\Activity 
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set gallery
     *
     * @param \AppBundle\Entity\Gallery $gallery
     * @return Media
     */
    public function setGallery(\AppBundle\Entity\Gallery $gallery = null)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \AppBundle\Entity\Gallery 
     */
    public function getGallery()
    {
        return $this->gallery;
    }
}
